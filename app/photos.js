const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const config = require('../config');
const Photo = require('../models/Photo');
const User = require('../models/User');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});


const createRouter = () => {
  const router = express.Router();

  router.get('/', async (req, res) => {
    if (req.query.userId) {
      const photos = await Photo.find({userId: req.query.userId});
      const user = await User.findOne({_id: req.query.userId});

      return res.send({photos, user});
    }

    const photos = await Photo.find()
      .populate({path: 'userId', select: 'username displayName'});

    return res.send(photos);
  });

  router.post('/', [auth, upload.single('image')], async (req, res) => {
    if (!req.file || !req.body.title) {
      res.send({message: 'Fill all required fields'});
    }

    const photoData = {
      title: req.body.title,
      image: req.file.filename,
      userId: req.user._id
    };

    const photo = new Photo(photoData);
    await photo.save();

    return res.send(photo);
  });

  router.delete('/', auth, async (req, res) => {
    const photo = await Photo.findOne({_id: req.query.photoId});

    if (!photo.userId.equals(req.user._id)) {
      return res.status(403).send({error: 'No access!'})
    }

    await photo.remove();

    const photos = await Photo.find({userId: req.user._id});

    return res.send(photos);
  });

  return router;
};

module.exports = createRouter;