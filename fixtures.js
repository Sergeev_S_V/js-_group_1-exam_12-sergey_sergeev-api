const mongoose = require('mongoose');

const config = require('./config');

const User = require('./models/User');
const Photo = require('./models/Photo');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('users');
    await db.dropCollection('photos');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [User1, User2] = await User.create({
    username: 'user1',
    password: '123',
  }, {
    username: 'user2',
    password: '123',
  });

  await Photo.create({
    title: 'first photo',
    image: 'user1photo1.jpg',
    userId: User1._id
  });

  await Photo.create({
    title: 'second photo',
    image: 'user1photo2.jpg',
    userId: User1._id
  });

  await Photo.create({
    title: 'third photo',
    image: 'user1photo3.jpg',
    userId: User1._id
  });

  await Photo.create({
    title: 'first photo',
    image: 'user2photo1.jpg',
    userId: User2._id
  });

  await Photo.create({
    title: 'second photo',
    image: 'user2photo2.jpg',
    userId: User2._id
  });

  await Photo.create({
    title: 'third photo',
    image: 'user2photo3.jpg',
    userId: User2._id
  });

  db.close();
});